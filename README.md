# SOLUTIONS FOR ***"ELOQUENT JAVASCRIPT"*** BOOK

This repository contains explanations and solutions for the exercises on the book, I hope that this material helps to someone that needs more explanations.

#### About the book:
- Name    → **ELOQUENT JAVASCRIPT - A Modern Introduction to Programming**
- Edition → **THIRD EDITION**
- Author  → **Marijin Haverbeke**
- Link    → **https://eloquentjavascript.net/**
