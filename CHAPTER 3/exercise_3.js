function countChar(str, chr) {
  let result = 0;
  for (let i = 0; i < str.length; i++) {
    if (str[i] == chr) result++; 
  }
  return result;
}

function countBs(str) {
  const chr = `B`;
  return countChar(str, chr);
}
