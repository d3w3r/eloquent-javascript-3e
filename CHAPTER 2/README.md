# Chapter 2 - Exercises: PROGRAM STRUCTURE

### LOPING A TRIANGLE
- The binding **chr** is a string with the value **#**, each iterations it receive one new character **#**.
- The `console.log()` inside the braces print the value of **chr** in a whole line at the console.
- We don't need to track the number of iterations with other binding because we can use the string **chr**, if we
use `.length` property of the strings and ask for its value each iteration.

### FIZZBUZZ
- First we have to test if the current number is divisible by the two numbers, and then we evaluate only with the 
others values.

### CHESSBOARD
- We have to create two for loops one for rows and other for column, and then execute a sum of the x and y to see if
current and see if the result is even or odd, if the value is odd add the character **#**.  

- [x] If you feel confortable, i challenge you to make the same exercise with only one for loop.
